PRAGMA foreign_keys = ON;

CREATE TABLE mitglied (
    mitgliedid INTEGER PRIMARY KEY,
    name TEXT,
    vorname TEXT,
    strasse TEXT,
    plz INTEGER,
    ort TEXT,
    emailadresse TEXT,
    handynr TEXT,    
    sportart INTEGER
);

CREATE TABLE sportart (
    sportartid INTEGER PRIMARY KEY,
    bezeichnung TEXT,
    FOREIGN KEY(sportartid) REFERENCES mitglied(sportart)
    );


INSERT INTO mitglied(0, 'Mueller', 'Julia', 'Rosenweg 12', 12345, 'Musterstadt', 'julia.mueller@example.com', '+49 123 4567890', 0)
INSERT INTO mitglied(1, 'Schmidt', 'Jonas', 'Birkenalle 7', 54321, 'Musterstadt', 'jonas.schmidt@example.com', '+49 987 65432100', 1) 
INSERT INTO mitglied(2, 'Wagner', 'Lisa', 'Ahornstrasse 3', 98765, 'Beispielstadt', 'lisa.wagner@example.com', '+49 456 1237890', 2)     
INSERT INTO mitglied(3, 'Becker', 'Tim', 'Lindenweg 15', 23456, 'Musterstadt', 'tim.becker@example.com', '+49 789 0123456', 3) 
INSERT INTO mitglied(4, 'Schulz', 'Sarah', 'Erlenstrasse 21', 34567, 'Beispielstadt', 'sarah.schulz@example.com', '+49 321 9876543', 4) 
INSERT INTO mitglied(5, 'Fischer', 'Max', 'Fichtenweg 8', 87456, 'Musterstadt', 'max.fischer@example.com', '+49 654 3210987', 5) 
INSERT INTO mitglied(6, 'Hoffmann', 'Laura', 'Buchenallee 6', 45678, 'Beispielstadt', 'laura.hoffmann@example.com', '+49 789 0123456', 6)     
INSERT INTO mitglied(7, 'Klein', 'Lukas', 'Eichenweg 4', 56789, 'Musterstadt', 'lukas.klein@example.com', '+49 987 6543210', 7)   
    
INSERT INTO sportart(0, 'Tennis')
INSERT INTO sportart(1, 'Fussball')
INSERT INTO sportart(2, 'Schwimmen')
INSERT INTO sportart(3, 'Basketball') 
INSERT INTO sportart(4, 'Volleyball')     
INSERT INTO sportart(5, 'Laufen')  
INSERT INTO sportart(6, 'Yoga')  
INSERT INTO sportart(7, 'Radfahren')
